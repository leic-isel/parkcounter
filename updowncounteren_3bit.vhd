----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:16:25 10/13/2020 
-- Design Name: 
-- Module Name:    updowncounteren_3bit - Structural 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity updowncounteren_3bit is
    Port ( incndec : in  STD_LOGIC;
           en : in  STD_LOGIC;
           operandA : in  STD_LOGIC_VECTOR (2 downto 0);
           R : out  STD_LOGIC_VECTOR (2 downto 0));
end updowncounteren_3bit;

architecture Structural of updowncounteren_3bit is
component MUX2_1 is
	Port ( S0 : in  STD_LOGIC_VECTOR (2 downto 0);
           S1 : in  STD_LOGIC_VECTOR (2 downto 0);
			  sel : in  STD_LOGIC;
           Y : out  STD_LOGIC_VECTOR (2 downto 0));
end component;

component adder3bit is
	Port ( A : in  STD_LOGIC_VECTOR (2 downto 0);
           B : in  STD_LOGIC_VECTOR (2 downto 0);
           Cin : in  STD_LOGIC;
           S : out  STD_LOGIC_VECTOR (2 downto 0);
           Cout : out  STD_LOGIC);
end component;

signal increment : std_logic_vector(2 downto 0);
signal operandB  : std_logic_vector(2 downto 0);
begin

U0:	MUX2_1
		port map (S0 => "111", S1 => "001", sel => incndec, Y => increment);
		
U1: 	MUX2_1
		port map (S0 => "000", S1 => increment, sel => en, Y => operandB);
		
U2:	adder3bit
		port map (A => operandA, B => operandB, Cin => '0', S => R, Cout => open);

end Structural;

