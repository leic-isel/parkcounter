----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:17:18 10/13/2020 
-- Design Name: 
-- Module Name:    FA - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

-- ENTITY - defenicao do componente, establecendo os sinais de entrada e saida ---
entity FA is
    Port ( A : in  STD_LOGIC;
           B : in  STD_LOGIC;
           Cin : in  STD_LOGIC;
           S : out  STD_LOGIC;
           Cout : out  STD_LOGIC);
end FA;

-- ARCHITECTURE - descricao da implementacao do componente -------------------------
-- Structural - descricao estrutural
-- Behavioral - descricao comportamental
architecture Behavioral of FA is
	-- declaracao dos sinais internos do componente
	signal xor_ab : std_logic;

begin
	-- descricao do circuito digital
	xor_ab <= A xor B;
	S <= xor_ab xor Cin;
	Cout <= (A and B) or (Cin and xor_ab);

end Behavioral;

