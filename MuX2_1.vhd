----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:05:52 10/13/2020 
-- Design Name: 
-- Module Name:    MuX2_1 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MuX2_1 is
    Port ( S0 : in  STD_LOGIC_VECTOR (2 downto 0);
           S1 : in  STD_LOGIC_VECTOR (2 downto 0);
			  sel : in  STD_LOGIC;
           Y : out  STD_LOGIC_VECTOR (2 downto 0));
end MuX2_1;

architecture Behavioral of MuX2_1 is

begin

	Y <= S0 when sel = '0' else S1;


end Behavioral;

